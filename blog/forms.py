from django import forms
from pagedown.widgets import PagedownWidget
from .models import Comment

class EmailPostForm(forms.Form):
	nombre = forms.CharField(max_length=25, label="", widget=forms.TextInput(attrs={'class':'form-control',
		'type':'text',
		'placeholder':'Nombre...'}))
	correo = forms.EmailField(label="", widget=forms.TextInput(attrs={'class':'form-control',
		'type':'email',
		'placeholder':'Correo...'}))
	para = forms.EmailField(label="", widget=forms.TextInput(attrs={'class':'form-control',
		'type':'email',
		'placeholder':'Para...'}))
	comentarios = forms.CharField(label="", required=False, widget=forms.Textarea(attrs={'class':'form-control',
		'type':'text',
		'placeholder':'Comentarios...',
		'rows':'3',}))

class CommentForm(forms.ModelForm):
	name = forms.CharField(max_length=80, label="", widget=forms.TextInput(attrs={'class':'form-control',
		'type':'text',
		'placeholder':'Nombre...'}))
	email = forms.EmailField(label="", widget=forms.TextInput(attrs={'class':'form-control',
		'type':'email',
		'placeholder':'Correo...'}))
	body = forms.CharField(max_length=150, label="", widget=forms.Textarea(attrs={'class':'form-control',
		'type':'text',
		'placeholder':'Comentario..',
		'rows':'3',}))
	#body = forms.CharField(widget=PagedownWidget(show_preview= False,attrs={'class':'form-control',
	#	'type':'text',
	#	'label':'',
	#	'placeholder':'Comentar Publicación...',
	#	'rows':'3',}))
	
	class Meta:
		model = Comment
		fields=('name', 'email', 'body')