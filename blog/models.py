from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.conf import settings
from taggit.managers import TaggableManager


class PublishedManager(models.Manager):
	def get_queryset(self):
		return super(PublishedManager, self).get_queryset().filter(status='published')

# Create your models here.
class Post(models.Model):
	STATUS_CHOICES = (('draft', 'Borrador'), ('published', 'Publicado'))
	title = models.CharField(max_length=250)
	slug = models.SlugField(max_length=250, unique_for_date='publish')
	author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='blog_posts')
	body = models.TextField()
	portada = models.ImageField(upload_to='portada-post', blank=True,null=True)
	image_card = models.ImageField(upload_to='card_post', blank=True, null=True)
	publish = models.DateTimeField(default=timezone.now)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
	objects = models.Manager()
	published = PublishedManager()
	tags = TaggableManager()

	class Meta:
		ordering = ('-publish',)

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('blog:post_detail',args = [self.slug])

class Comment(models.Model):
	post = models.ForeignKey(Post, related_name="comments")
	name = models.CharField(max_length=80)
	email = models.EmailField()
	body = models.CharField(max_length=150)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	active = models.BooleanField(default=True)

	class Meta:
		ordering = ('created',)

	def __str__(self):
		return 'Comentado por {} en {}'.format(self.name, self.post)


