from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$',views.MainView.as_view(), name = "main"),
	url(r'^registro/$',views.RegisterView.as_view(), name = "register"),
	url(r'^terminos-y-condiciones/$',views.TermsAndConditionsView.as_view(), name = "terms"),
	url(r'^nosotros/$',views.AboutUsView.as_view(), name = "about"),
]