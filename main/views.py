from django.shortcuts import render
from django.views.generic import View
# Create your views here.


class MainView(View):
	def get(self,request):
		template_name = 'main.html'
		return render(request,template_name)

class RegisterView(View):
	def get(self,request):
		template_name = 'register_on_build.html'
		return render(request,template_name)

class TermsAndConditionsView(View):
	def get(self,request):
		template_name = 'terms.html'
		return render(request,template_name)

class AboutUsView(View):
	def get(self,request):
		template_name = 'about_us.html'
		return render(request,template_name)
