from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from travels.models import Travel



class Origin(models.Model):
	name = models.CharField(max_length = 140, blank=True,null=True)
	lat = models.FloatField(null=True,blank=True)
	lng = models.FloatField(null=True, blank=True)
	origin_travel = models.ForeignKey(Travel, related_name='travel_origin',blank=True,null=True)

	def __str__(self):
		return '%s' % (self.origin_travel.title)

class Destination(models.Model):
	name = models.CharField(max_length = 140, blank=True,null=True)
	lat = models.FloatField(null=True,blank=True)
	lng = models.FloatField(null=True,blank=True)
	destination_travel = models.ForeignKey(Travel,related_name='travel_destination',blank=True, null=True)

	def __str__(self):
		return self.destination_travel.title