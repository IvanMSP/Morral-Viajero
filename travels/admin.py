from django.contrib import admin
from django.db import models
from pagedown.widgets import AdminPagedownWidget
from .models import Travel,Price,Contact,Category,Date,Discount



class CategoryAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('name',)}

class PriceInLine(admin.StackedInline):
	formfield_overrides = {
		models.TextField:{'widget': AdminPagedownWidget},
	}
	model = Price


class ContactInLine(admin.StackedInline):
	model = Contact

class PriceAdmin(admin.ModelAdmin):
	formfield_overrides = {
		models.TextField:{'widget': AdminPagedownWidget},
	}
admin.site.register(Price,PriceAdmin)

class TravelAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('title',)}
	formfield_overrides = {
		models.TextField:{'widget': AdminPagedownWidget},
	}
	inlines = [PriceInLine, ContactInLine]





admin.site.register(Category,CategoryAdmin)
admin.site.register(Travel,TravelAdmin)
admin.site.register(Discount)
admin.site.register(Contact)
admin.site.register(Date)

