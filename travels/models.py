from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from datetime import datetime



class Category(models.Model):
	name = models.CharField(max_length = 50, db_index=True)
	slug = models.SlugField(max_length = 50, db_index=True, unique=True)

	class Meta():
		ordering = ('name',)
		verbose_name = 'category'
		verbose_name_plural = 'categories'

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse('travel:travels_by_category', args = [self.slug])



class Travel(models.Model):
	category = models.ForeignKey(Category, related_name = 'category', blank=True, null=True)
	title = models.CharField(max_length=200, blank = True , null= True, db_index=True)
	slug = models.SlugField(max_length = 200,db_index=True) 
	description = models.TextField()
	recommendations = models.TextField()
	itinerary = models.TextField()
	assistants = models.IntegerField()
	image_port = models.ImageField(upload_to ='port-travels', blank=True,null=True)
	image_detail = models.ImageField(upload_to = 'detail-images', blank=True, null=True)
	image_card = models.ImageField(upload_to = 'card-images', blank=True, null=True)
	available = models.BooleanField(default=True)

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('travel:travel_detail', args = [self.slug])

	def get_first_date(self):
		first_date = Date.objects.filter(travel_date = self)[:1]
		return first_date

	def get_percentage(self):
		percentage = Discount.objects.filter(travel_discount = self)[:1]
		return percentage

class Date(models.Model):
	name = models.CharField(max_length=20, blank=True,null=True, db_index=True)
	date_time = models.DateTimeField(blank=True,null=True)
	available = models.BooleanField(default=True)
	travel_date= models.ForeignKey(Travel,related_name='travel_datee', blank=True,null=True)

	def __str__(self):
		return self.travel_date.title

class Price(models.Model):
	travel_price = models.ForeignKey(Travel,on_delete=models.CASCADE, blank=True,null=True, related_name='price')
	cost = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
	description_price = models.TextField()

	def __str__(self):
		return '{} de {}'.format(self.cost, self.travel_price.title)

class Contact(models.Model):
	travel_contact = models.OneToOneField(Travel,on_delete=models.CASCADE, blank=True,null=True, related_name='contact')
	fb_id = models.CharField(max_length=200)
	tw_id = models.CharField(max_length=200)
	yt_id = models.CharField(max_length=200)
	web_url = models.URLField(max_length=500,blank=True,null=True)
	email = models.EmailField(max_length=100, blank=True,null=True)
	whatsapp_one = models.IntegerField(null=True)
	whatsapp_two = models.IntegerField(null=True)

	def __str__(self):
		return self.travel_contact.title

class Discount(models.Model):
	travel_discount = models.ForeignKey(Travel, on_delete = models.CASCADE, blank=True, null=True, related_name = 'discount')
	travel_price_discount = models.OneToOneField(Price, related_name = 'price_discount')
	value = models.DecimalField(blank=True,null=True, decimal_places = 2, max_digits = 5)
	active = models.BooleanField(default=True)

	def __str__(self):
		return self.travel_discount.title

	
	def calculate_disccount(self):
		desc_sub = self.travel_price_discount.cost * self.value / 100
		total = self.travel_price_discount.cost - desc_sub
		return int(total)

		# porcent = (desc_sub * 100)/ self.travel_price_discount.cost
		#return '{} - con el {}% de descuento '.format(float(total), int(porcent))



