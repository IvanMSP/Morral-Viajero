from django import template
from django.db.models import Count

from django.utils.safestring import mark_safe
import markdown


register = template.Library()


from ..models import Travel,Date

@register.simple_tag
def num_travels():
	return Travel.objects.count()

	
@register.inclusion_tag('travel/travels_recents.html')
def show_recents_travels():
	recents_travels = Travel.objects.filter(available = True).order_by('travel_datee__date_time')[:4]
	return {'recents_travels':recents_travels,}

@register.filter(name='markdown')
def markdown_format(text):
	return mark_safe(markdown.markdown(text))
