from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^travels/$', views.Travel_List.as_view(),name="travel_list"),
    url(r'^travel/(?P<slug>[-\w]+)/$',views.Travel_Detail.as_view(), name="travel_detail"),
    url(r'^travels-by-category/(?P<category_slug>[-\w]+)/$',views.Travel_List.as_view(),name="travels_by_category"),
]
