from django.shortcuts import render,get_object_or_404
from django.views.generic import View
from .models import Travel,Price,Contact,Category,Date,Discount
from places.models import Origin,Destination
#Paginación
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger



class Travel_List(View):
	def get(self,request,category_slug = None):
		template_name = 'travel/travel-list.html'
		categories = Category.objects.all()
		object_list = Travel.objects.all()
		category = None
		if category_slug:
			category = get_object_or_404(Category, slug = category_slug)
			object_list = object_list.filter(category = category)
			
		paginator = Paginator(object_list,4)
		page = request.GET.get('page')
		try:
			travels = paginator.page(page)
			
		except PageNotAnInteger:
			travels = paginator.page(1)
		except EmptyPage:
			travels = paginator.page(paginator.num_pages)
		context = {
			'page':page,
			'category':category,
			'categories':categories,
			'travels':travels,
			
		}
		
		return render(request,template_name,context)


class Travel_Detail(View):
	def get(self,request,slug):
		template_name = 'travel/travel-detail.html'
		travel = get_object_or_404(Travel, slug = slug)
		prices = Price.objects.all().filter(travel_price = travel)
		contact = Contact.objects.all().filter(travel_contact = travel)
		dates = Date.objects.all().filter(travel_date = travel).order_by('date_time')
		origins = Origin.objects.all().filter(origin_travel = travel)
		destinations = Destination.objects.all().filter(destination_travel = travel)
		discounts = Discount.objects.all().filter(travel_discount = travel)
		

		context = {
			'travel': travel,
			'prices': prices,
			'contact': contact,
			'dates':dates,
			'origins':origins,
			'destinations':destinations,
			'discounts':discounts,
		
		}
		return render(request,template_name,context)
